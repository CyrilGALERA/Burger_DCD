<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/logo.png">
    <link rel="stylesheet" href="./CSS/styleA.css">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>ADMIN: produits</title>
</head>

<body>
    
    <?php
    include 'a_debug.php';
    include 'v0_header_admin.php';
    include 'c2_produits.php';
    ?>

    <main class="padding-H10 center-elt col">

        <nav class="dx80 ligne box-blanche padding-H10 axe1-sp-around axe2-center">
            <ul>
                <li>
                    <a href="./v2_commandes.php">
                        <div class="box-centree"><h2>CATEGORIES</h2>
                            <h3>des produits</h3>
                        </div>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="./v2_commandes.php">
                        <div class="box-centree"><h2>PRODUITS</h2>
                            <h3>De type "Réservation"</h3>
                        </div>
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="./v2_commandes.php">
                        <div class="box-centree"><h2>PRODUITS</h2>
                            <h3>à la Carte</h3>
                        </div>
                    </a>
                </li>
            </ul>
        </nav>

        <!-- form AJOUTER / MODIFIER  ANNULER =========================== -->
        <div class="dx80 col box-blanche margin-V10 padding-H10">
        <h2> PRODUITS: Ajouter / Modifier / Supprimer </h2>
        <!-- <div class="border-1b padding-V10 padding-H10"> -->
        <div class="border-1b padding-V10">
            <!-- Récupérer produit_select si un produit à été sélectionner par btn submit VoirModif -->
            <?php
                $action = 'AJOUTER';
                if(isset($produit_select)){
                    $action = 'MODIFIER';
                }
            ?>

            <!-- Afficher le formulaire d' $action = AJOUTET / MODIFIER (ou ANNULER) -->
            <form id="form-prod-carte" class="ligne dx90" enctype="multipart/form-data" action="c2_produits.php" method="post">
                <!-- Demander info produit (ou afficher produit_select if isset -->
                <div class="col flex4">
                    <div class="ligne axe1-start">
                        <label for="nom"> Produit: </label>
                        <input type="text" name="nom" value=
                            "<?= isset($produit_select)? $produit_select['nom'] : ""; ?>">
                    </div>

                    <div class="ligne axe1-start">
                        <label for="id_cat_prod">Catégorie:</label>
                        <select name="id_cat_prod" id="id_cat_prod"
                            value="<?= isset($produit_select)? $produit_select['id_cat_prod'] : ""; ?>"
                        >
                            <!-- Lister toutes les options de Table CategorieProd -->
                            <option value="">Reserver / Carte</option>
                            <?php $tab_cat = getTable('CategorieProd');?>
                            <?php foreach($tab_cat as $cat_i) { ?>
                                <option value="<?php echo $cat_i['id']?>"  
                                    <?php if($produit_select && $produit_select['id_cat_prod'] === $cat_i['id']) echo "selected"; ?>
                                >   <?php echo $cat_i['nom']?>  </option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="ligne axe1-start">
                        <label for="imgFile"> Image: </label>
                        <input type="file" name="imgFile" accept="image/png, image/jpeg, image/jpg,"
                            value="<?= isset($produit_select)? $produit_select['imgFile'] : ""; ?>">
                    </div>
                    <div class="ligne axe1-start">
                        <label for="prix"> Prix --.-- € </label>
                        <input type="number" step="0.01" name="prix"
                            value="<?= isset($produit_select)? $produit_select['prix'] : ""; ?>">
                    </div>
                    <div class="ligne axe1-start">
                        <label for="quantite"> Quantité: </label>
                        <input type="number" step="0.001" name="quantite"
                            value="<?= isset($produit_select)? $produit_select['quantite'] : ""; ?>">
                    </div>
                </div>

                <div class="col flex4 ">
                    <label for="descript">Produit, description:</label>
                    <textarea id="descript" name="descript" rows="5" cols="33"><?= 
                        isset($produit_select)? $produit_select['descript'] : ""; ?>
                    </textarea>
                </div>
                    
                <!-- BOUTON AJOUTER ou ANNULER ---------->
                <!-- Saisir id et action -->
                <div class="col flex1 axe1-center padding-H10">
                    <input type="hidden" name="id" value="<?= isset($produit_select)? $produit_select['id'] : ""; ?>">
                    <input class="btn flex1 col axe1-center axe2-center margin-H5" type="submit" name="<?= $action; ?>"
                        value="<?= $action; ?>">
                    <a class="btn flex1 col axe1-center axe2-center margin-H5" href="v2_produits.php">ANNULER</a>
            </div>
            </form>
        </div>
        </div>

        <div class="col box-blanche margin-V10 padding-H10">
            <!-- TITRE PRODUITS + select Catégorie ========================================== -->
            <div class="ligne">
                <h2> PRODUITS:... </h2>
                <!-- form get selected
                selected => href v2_produits?id_cat=... -->
                <div class="ligne axe2-center">
                    <label for="id_cat_prod"> CATEGORIE:... </label>
                    <select name="id_cat_prod" id="id_cat_prod">
                        <!-- Lister toutes les options de Table CategorieProd -->
                        <option value="">Reserver / Carte</option>
                        <?php $tab_cat = getTable('CategorieProd');?>
                        <?php foreach($tab_cat as $cat_i) { ?> 
                            <option value="<?php echo $cat_i['id']?>"  
                            > <?php echo $cat_i['nom']?>  </option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <!-- BLOC LISTE DES PRODUITS  ============================================= -->
            <div class="border-1b padding-V10 padding-H10">
                <?php $tab_produits = getTable('Produits');?>
                <?php foreach($tab_produits as $produit_i) { ?>
                <div class="ligne axe2-center padding-V10 border-bot">
                    <!-- Afficher Produit: info -->
                    <img class="flex2 imgSize70h80w imgContaint "
                        src="<?php  echo "./IMAGES/produits/".$produit_i[ "imgFile" ] ?>" alt="IMG Prod_i">

                    <h4 class="flex2"> <?php  echo $produit_i[ "nom" ] ?> </h4>
                    <h4 class="flex1"> <?php  echo $produit_i[ "prix" ] ?> € </h4>
                    <h4 class="flex1"> <?php  echo $produit_i[ "quantite" ] ?> </h4>
                    <h4 class="flex3"> <?php  echo $produit_i[ "descript" ] ?> </h4>

                    <!-- Afficher Produit: action VOIR-pour-MODIFF / SUPPR -->
                    <form class="flex2 col axe1-center padding-H10" action="c2_produits.php" method="post">
                        <a class="btn col axe1-center axe2-center margin-H5"
                            href="v2_produits.php?id=<?= $produit_i['id']; ?>">MODIFIER</a>

                        <input type="hidden" name="id" value="<?= $produit_i['id']; ?>">
                        <input class="btn col axe1-center axe2-center margin-H5" type="submit" name="SUPPRIMER"
                            value="SUPPRIMER">
                    </form>

                </div> <!-- Fin div ligne produit_i "prod_Fiche"-->
                <?php  } ?>
            </div> <!-- Fin div tab_produits "stockBox" -->
        </div> <!-- Fin div box-blanche -->

    </main>
    <?php include 'v0_footer.php';?>
</body>
</html>