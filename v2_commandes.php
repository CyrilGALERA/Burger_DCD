<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/logo.png">
    <link rel="stylesheet" href="./CSS/styleA.css">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>ADMIN: Accueil</title>
</head>

<body>
    
    <?php
        include 'a_debug.php';
        include 'v0_header_admin.php';
    ?>

    <main>
        <h2> ADMIN: commandes </h2>
    </main>

    <?php include 'v0_footer.php';?>
</body>
</html>