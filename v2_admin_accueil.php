<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/logo.png">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="stylesheet" href="./CSS/styleA.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>ADMIN: Accueil</title>
</head>

<body>
    
    <?php
    include 'a_debug.php';
    include 'v0_header_admin.php';
    ?>

    <main>
            <div>
            <h2> ESPACE ADMIN: accueil </h2>

            <nav class="ligne">
                <ul class="box-blanche">
                    <li>
                        <!-- <a class="Nav_Page_i" href="./indexx.php">Retour Site Web</a> -->
                        <a href="./v2_produits.php"> <h3>INFORMATION</h3> </a>
                        <p> Date d'ouverture </p>
                        <p>Horraire</p>
                        <p>Adresse</p>

                        <h4>RESTAURANT</h4>
                        <p>Notre équique</p>
                        <p>Notre savoir faire</p>
                        <p>Notre Histoire</p>

                        <h4>PUBLICITE</h4>
                        </p>Message promotionnel</p>
                        </p>Mis en avant des produit</p>
                    </li>
                </ul>
                <ul class="box-blanche">
                    <li class="box_blanche">
                        <a href="./v2_produits.php"> <h3>PRODUITS</h3> </a>
                        </p>Carte</p>
                        </p>Réservation Commandes</p>
                    </li>
                </ul>
                <ul class="box-blanche">
                    <li class="box_blanche">
                        <a href="./v2_commandes.php"> <h3>COMMANDER RESERVER</h3> </a>
                        <h4>RESERVER:</h4>
                        <p>Table 1pers, 2pers..., la salle grill, le restau</p>
                        
                        <h4>VENTE A EMPORTER:</h4>

                        <h4>VENTE EN LIVRAISON:</h4>
                        </p>Commander via nos partenaires</p>
                        <p>Uber, Delivroo...</p>
                    </li>
                </ul>
            </nav>
            </div>

    </main>

    <?php include 'v0_footer.php';?>

</body>
</html>