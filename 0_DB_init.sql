
-- Use script: sudo mysql < 0_DB_init.sql;

-- -------------------------------------------------------------
-- DATABASE:    BDD_BurgerDCD;
-- TABLE:       Produits
--              CategorieProd
--              Administrateur
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use BDD_BurgerDCD;
-- show tables;
-- describe Produits;
-- select * from Produits;
-- quit;
-- exit;
-- -------------------------------------------------------------

DROP DATABASE IF EXISTS BDD_BurgerDCD;

CREATE DATABASE BDD_BurgerDCD;

GRANT ALL PRIVILEGES ON BDD_BurgerDCD.* TO 'u_cyril'@'localhost';

USE BDD_BurgerDCD;

SELECT "DATABASE ClickAndCollect Créée pour user u_cyril";
SELECT "(Modifier le user ligne 28: GRANT...)";



-- NEW TABLE -----------------------------
-- 
CREATE TABLE CategorieProd (
    id          INT unsigned auto_increment PRIMARY KEY,
    nom         varchar(100),
    descript    varchar(255)
);

CREATE TABLE Produits (
    id          INT unsigned auto_increment PRIMARY KEY,
    nom         varchar(100),
    imgFile     varchar(100),
    descript    varchar(255),
    prix        FLOAT DEFAULT 0.0,
    quantite    INT,
    id_cat_prod INT unsigned,
    FOREIGN KEY (id_cat_prod) REFERENCES CategorieProd(id)
);

-- Historique Complet de toute la Pub Mis en avant Produit
-- Mis en avant de 3 ou 4 produits Maxi
CREATE TABLE PubProduits (
    id          INT unsigned auto_increment PRIMARY KEY,
    id_prod     INT unsigned,
    msg         varchar(255),
    date_j      DATETIME,
    FOREIGN KEY (id_prod) REFERENCES Produits(id)
);

CREATE TABLE Administrateur (
    id          INT unsigned auto_increment PRIMARY KEY,
    pseudo      varchar(50),
    passwd    varchar(50)  
);



