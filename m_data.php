<?php 

// RAPPEL BDD BDD_BurgerDCD
//    

include 'a_debug.php';

// Connexion à la DB ===========================================
$host = 'localhost';
$bdd = 'BDD_BurgerDCD';
$user = 'u_cyril';
$pwd = 'pwd_cyril';

$pdo = new PDO("mysql:host=$host;dbname=$bdd;", $user, $pwd);

// Variable globale ===========================================
$cat_ReservId = [1, 2, 3, 4];
$cat_ReservNom = ['Restau', 'Salle', 'Table1', 'Table2'];

$cat_CarteId = [5, 6, 7, 8, 9];
$cat_CarteNom = ['Burgers', 'Accompagnant', 'Boissons', 'Desserts', 'Menu'];
// En fait il faudrait avoir 2 Categories 2.1 Reservation 2.2 Carte qui hérite les deux de 1 Categorie Mere
// Je pourrais aussi les initialiser...

// CRUD =========================================================
// Create 
// Read
// Update
// Delete

// CRUD =========================================================
// Create 
function createData($strTableBDD, $tab_colStr, $tab_colValue) {
    global $pdo;
    // echo "</p> createData: $tab_colStr ";
    // echo "</p> createData: $tab_colValue ";

    // Verif: nb elt dans $tab_colStr = nb elt dans $tab=colValue
    $nbStr = count($tab_colStr);
    $nbVal = count($tab_colValue);
    if( $nbStr !== $nbVal ) {
        echo "</p> m_data.php: createData()" ;
        echo "nb elt dans les tab_colStr et tab_colValue ne correspondent pas: $nbStr != $nbVal";
        var_dump($tab_colStr);
        var_dump($tab_colValue);
        return 0;
    }

    // Créer:   "colStr0, colStr1, ... colStrN  ";
    //          "?      , ?      , ...  ?       ";
    $col_names = "";
    $nb_interrogations = "";
    for($i=0; $i<$nbStr-1; $i++) {
        $col_names          = $col_names.$tab_colStr[$i].", ";
        $nb_interrogations  = $nb_interrogations."?, ";
    }
    $col_names          = $col_names.$tab_colStr[$i]."";
    $nb_interrogations  = $nb_interrogations."?";

    // sql req
    $sql = "INSERT INTO $strTableBDD ($col_names) VALUES ($nb_interrogations) ";
    echo "</p> $sql";
    $req = $pdo->prepare($sql);
    $r = $req->execute( $tab_colValue );
    return $pdo->lastInsertId();
}

// CRUD =========================================================
// Read

function getAllColName($strTable) { 
    global $pdo;
    $sql = "DESCRIBE $strTable" ;
    $stm = $pdo->prepare($sql);
    $stm->execute();
    return $stm->fetchAll();
}

function getTable($strTable) { 
    global $pdo;
    $sql = "SELECT * FROM $strTable";
    // $sql = "SELECT * FROM ?"; marche pas
    $stm = $pdo->prepare($sql);
    // $stm->execute( [$strTable] ); marche pas non plus...
    $stm->execute( );
    return $stm->fetchAll(PDO::FETCH_ASSOC);
}

function getDataId($strTable,$id) {
    global $pdo;
    $sql = "SELECT * FROM $strTable WHERE id = $id";
    $stm = $pdo->prepare($sql);
    $stm->execute([$id]);
    return $stm->fetch();
}

function getAllDataWhere($strTable, $colStr, $colValue) {
    global $pdo;
    $sql = "SELECT * FROM $strTable WHERE $colStr = ? ";
    $stm = $pdo->prepare($sql);
    $stm->execute([$colValue]);
    return $stm->fetchAll();
}

function getAllProduitsCat($catNom) {
    global $pdo;
    $sql = "SELECT * FROM Produits INNER JOIN CategorieProd
            ON Produits.id_cat_prod = CategorieProd.id
            WHERE CategorieProd.nom = ?
            "; 
    // echo "</p> sql = $sql";

    $req = $pdo->prepare($sql);
    // echo "</p> req = $req";
    // var_dump($req);

    $req->execute( [$catNom] );
    // echo "</p> req = $req";
    // echo "</p>";

    // var_dump($req);

    $tab = $req->fetchAll();
    // echo "</p>";

    // var_dump($catNom);
    // var_dump($tab);
    return $tab;
}


// CRUD =========================================================
// Update
function updateData($strTableBDD, $tab_colStr, $tab_colValue, $id) {
    global $pdo;
    // Verif: nb elt dans $tab_colStr = nb elt dans $tab_colValue
    $nbStr = count($tab_colStr);
    if( $nbStr !== count($tab_colValue) ) {
        echo "</p> m_data_func.php: updateData()" ;
        echo "nb elt dans les tab_colStr et tab_colValue ne correspondent pas: $nbStr != $nbVal";
        var_dump($tab_colStr);
        var_dump($tab_colValue);
        return 0;
    }

    // Req SQL: 
    // $sql = "colStr1 =?, colStr2 =?, colStr3..."
    //
    $sql = " ";
    for($i=0; $i<$nbStr-1; $i++) {
        $sql = $sql.$tab_colStr[$i]." = ?, ";
    }
    //
    $sql = $sql.$tab_colStr[$i]." = ? ";
    //
    $sql = "UPDATE $strTableBDD SET ".$sql." WHERE id = ?";
    $tab_colValue[] = $id;
    $stm = $pdo->prepare($sql);
    $stm->execute( $tab_colValue );
    return $stm->fetch();
    // return getDataId($strTableBDD, $id);
}

// CRUD =========================================================
// Delete
function deleteData($strTable, $id)
{
    global $pdo;
    $sql = "DELETE FROM $strTable WHERE id = ?";
    $stm = $pdo->prepare($sql);
    return $stm->execute([$id]);
}
?>