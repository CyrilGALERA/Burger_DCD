<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="./images/logo.png">
    <link rel="stylesheet" href="./styleA.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>Gestion Produits</title>
</head>

<body>
    

    <main>
        <div class="leftBox">
            <h2> - Ajouter un Plat - </h2>
            <form id="form_DB_produits_modif">

                <!-- Ligne: CATEGORIE -->
                <label for="Categorie-select" id="Cate_Tx">Catégorie:</label>

                <select name="Categorie" id="Categorie-select">
                    <option value="">--Choix de la catégorie--</option>
                    <option value="dog">CAT: Burgers</option>
                    <option value="cat">CAT: Boissons</option>
                    <option value="hamster">CAT: Desserts</option>
                    <option value="parrot">CAT: Autres</option>
                </select>

                <!-- Ligne: NOM PRODUIT -->
                <label for="Nom_text" id="Nom_tx">Nom:</label>
                <input type="text" id="Nom_input" name="Nom_text" minlength="2" maxlength="30" size="10" required>

                <!-- Ligne: IMAGE -->
                <label for="Prod_img">Image:</label>
                <input type="file" id="Prod_img" name="Prod_img" accept="image/png, image/jpeg">

                <!-- Ligne: DESCRIPTION PRODUIT -->
                <label for="Description">Description:</label>
                <textarea id="Description" name="Description" rows="5"
                    cols="20">Ecrire ici la déscription du produit...</textarea>

                <!-- Ligne: PRIX: -->
                <label for="Prix">Prix:</label>
                <input type="number" id="Prix" name="Prix" min="0" max="100000">

                <!-- Bouton VALIDER -->
                <button class="btn" id="Btn_Valider">Valider</button>
            </form>
        </div>

        <div class="rightBox">
            <div class="EnAttente">

                <h2 id="Produit_dispo"> - Produits disponibilités - </h2>

                <h2 id="Produit_avant"> - Mettre en avant un Produit - </h2>

            </div>

            <a id="BtnResa" href="reservation.php">Voir Reservation</a>

        </div>
    </main>

</body>

</html>