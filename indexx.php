<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./images/logo.png">
    <link rel="stylesheet" href="./CSS/styleG.css">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>CARTE</title>
</head>

<body id="Top">
    
    <?php
    include 'a_debug.php';
    include 'v0_header.php';
    include 'm_data.php';
    ?>

    <main class="padding-H10 col">
        
        <!-- TITRE Notre Carte ET NavBar2 ====================================== -->
        <div class="box-blanche col axe1-center axe2-center">
            <h2>- Notre Carte -</h2>
            
            <nav class="dx100 margin-H10">
                <ul class="ligne axe1-sp-around axe2-center">
                    <?php foreach($cat_CarteId as $cat_i) { ?>
                    <li class="btn">
                        <?php $catNom = getDataId('CategorieProd',$cat_i)['nom'] ?>
                        <a  href=<?php echo '#titre-'.$catNom?> > <?php echo $catNom ?> </a>
                    </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>

        <div class="box-marron ombre margin-H50 margin-V5 col">
            <h3 class="center-txt"> Nos offres du moment </h3>
            <div class="ligne axe1-sp-around axe2-center">
                <?php $tab_pubProduit = getTable('PubProduits');
                // Prend toute la pub, ne prendre que le Top 3 DESC
                // $tab_pubProduit = getTableLast('PubProduits',3);

                foreach($tab_pubProduit as $pub_i) { ?>
                    <div class="col axe1-center flex1 padding-H20">
                        <div class="center-elt">
                            <?php $produit = getDataId('Produits', $pub_i['id_prod']); ?>
                            <img class="imgContaint  imgSize140h160w"
                                src="<?= './IMAGES/produits/'.$produit['imgFile'] ?>" alt="ImgPub">
                        </div>
                        <div class="ligne axe1-sp-around axe2-center">
                            <h4> <?= $produit['nom']; ?> </h4>
                            <h4> <?= $produit['prix'].' €'; ?>  </h4>
                        </div>
                        <div>
                            <h5 class="center-txt"> <?= $pub_i['msg'] ?> </h5>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="box-blanche margin-V5 ligne axe1-sp-around axe2-center">
            <h3 id="titre-Burgers"> Burgers </h3>
            <div class="btn">
                <a  href="#Top" > Top </a>
            </div>
        </div>

        <div class="box-marron list margin-H50 margin-V5 padding-H20 padding-V20">
                <?php $tab_burgers = getAllProduitsCat('Burgers');
                foreach($tab_burgers as $burger_i) { ?>

                <div class="col padding-H10 padding-V10">
                    <img class="imgContaint  imgSize140h160w"
                        src="<?= './IMAGES/produits/'.$burger_i['imgFile'] ?>" alt="Img Boisson">
                    <div class="col padding-H20">
                        <div class="ligne axe1-sp-between">
                            <h4> <?= $burger_i['nom']; ?> </h4>
                            <h5> <?php if( $burger_i['quantite']<1) echo "INDISPONIBLE"; ?> </h5>
                        </div>
                        <h4> <?= $burger_i['prix'].' €'; ?>  </h4>
                        <h5> <?= $burger_i['descript']; ?>  </h5>
                    </div>  
                </div>
                <?php } ?>
        </div>

        
        <div class="box-blanche margin-V5 ligne axe1-sp-around axe2-center">
            <h3 id="titre-Boissons"> Boissons </h3>                            
            <div class="btn">
                <a  href="#Top" > Top </a>
            </div>
        </div>

        <div class="box-marron list margin-H50 margin-V5 padding-H20 padding-V20">
                <?php $tab_boissons = getAllProduitsCat('Boissons');
                foreach($tab_boissons as $boisson_i) { ?>

                <div class="col padding-H10 padding-V10">
                    <img class="imgContaint  imgSize140h160w"
                        src="<?= './IMAGES/produits/'.$boisson_i['imgFile'] ?>" alt="Img Burger">
                    <div class="col padding-H20">
                        <div class="ligne axe1-sp-between">
                            <h4> <?= $boisson_i['nom']; ?> </h4>
                            <h5> <?php if( $boisson_i['quantite']<1) echo "INDISPONIBLE"; ?> </h5>
                        </div>
                        <h4> <?= $boisson_i['prix'].' €'; ?>  </h4>
                        <h5> <?= $boisson_i['descript']; ?>  </h5>
                    </div>  
                </div>
                <?php } ?>
        </div>

        
           

    </main>

    <?php include 'v0_footer.php';?>

</body>

</html>