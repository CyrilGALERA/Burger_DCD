<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/logo.png">
    <link rel="stylesheet" href="./CSS/styleG.css">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>COMMANDE</title>
</head>

<body>
    
    <?php
    include 'a_debug.php';
    include 'v0_header.php';
    include 'm_data.php';
    ?>

    <main class="padding-H10 col">
        
        <!-- TITRE Notre Carte ET NavBar2 ====================================== -->
        <div class="box-blanche ligne axe1-sp-around axe2-center">
            <div class="col">
                <h2>- Réserver -</h2>
                <h3 class="center-txt"> Tel: 06 03 66 27 41 </h3>
                <div class="ligne axe1-sp-around axe2-center">
                    <h3 class="padding-H10"> Le Restau </h3>
                    <h3 class="padding-H10"> Une Salle </h3>
                    <h3 class="padding-H10"> Une Table </h3>
                </div>
                <h5> Bouton Formulaire à Faire </h5>
            </div>

            
            <div class="col">
                <h2>- Commander -</h2>
                <h3> Ventes à emporter (Btn) </h3>
                <h3> Livraison: (Uber ...) </h3>
                <h5> &nbsp; </h5>

                <!-- <h3> &nbsp; </h3> -->
                <!-- <div class="ligne axe1-sp-around axe2-center">
                    <h3> Le Restau </h3>
                    <h3> Une Salle </h3>
                    <h3> Une Table </h3>
                </div>
                <h5> Bouton Formulaire à Faire </h5> -->
            </div>
        </div>


        <!-- <form action="./login.php" method="post"> -->
        <form id="form_reservation">

            <!-- Ligne: NOM -->
            <label for="Nom_text" id="Nom_tx">Nom:</label>
            <input type="text" id="Nom_input" name="Nom_text" minlength="2" maxlength="30" size="10" required>

            <!-- Ligne: TEL -->
            <label for="Tel_text" id="Tel_tx">Téléphone:</label>
            <input type="tel" id="Tel_input" name="Tel_text" pattern="[0-9]{8}" minlength="8" maxlength="8" required>

            <!-- Ligne: DATE -->
            <label for="Date_text" id="Date_tx">Date:</label>
            <input type="date" id="Date_input" name="Date_text" value="2021-11-25" min="2021-11-25" max="2021-12-31"
                required>

            <!-- MIDI - SOIR -->
            <div id="Midi_box" class="box_time">
                <label for="midi" id="Midi_tx">Midi</label>
                <input type="checkbox" id="midi_input" name="midi">
                <!-- checked> -->
            </div>

            <div id="Soir_box" class="box_time">
                <label for="soir" id="Soir_tx">Soir</label>
                <input type="checkbox" id="soir_input" name="soir">
                <!-- checked> -->
            </div>

            <!-- Bouton RESERVER -->
            <button class="btn" id="Btn_Reserver">Réserver</button>
        </form>
    </main>

    <?php include 'v0_footer.php';?>

</body>
</html>