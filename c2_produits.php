<?php
    include 'm_data.php';


// Return   - a Array of values of all $_POST['inputNames_i'] 
// Return   - NULL if prob (Check isset and if value !="")
//
function getPOST($inputNames) {
        $inputValues = [];

        foreach($inputNames as $name_i) {
            echo "</p> name_i  = $name_i -- _POST[$name_i] = $_POST[$name_i]";
            if( isset($_POST[$name_i]) && $_POST[$name_i] !== "") {
                array_push($inputValues, $_POST[$name_i]);
            } else {
                echo "</p> getPOST";
                var_dump($_POST[$name_i]);
                return NULL;
            }
        }
        echo "</p> getPOST";
        var_dump($inputValues);
        return $inputValues;
}
// Return   - the(one) value of FILES[$inputName]
//              also upload the file in $pathDirName (ex: "./images") 
// Return   - NULL if prob (Check isset and if value !="")
//
function getUploadFILES($inputName,$pathDirName) {
    // get $_FILES
    if( isset($_FILES[$inputName]) && $_FILES[$inputName]['name'] !== "") {
        $inputValue = $_FILES[$inputName];
    } else {
        echo "</p> getFILES";
        var_dump($_FILES[$inputName]['name']);
        return NULL;
    }
    
    // upload $_FILES
    $nomFichier = $_FILES['imgFile']['name'];
    
    // $toPathFile = 
    if (move_uploaded_file($_FILES[$inputName]['tmp_name'], $pathDirName.$nomFichier)) {
        print "Téléchargé avec succès!";
    } else {
        // Echec: attention quand les fichiers sont trop lourd !!
        print "Échec du téléchargement!. Le fichier est peut être trop lourd.";
    }
    //
    return $nomFichier;
}

// CONTROL

// CRUD =========================================================
// Create 
// Read
// Update
// Delete

// CRUD =========================================================
// Create 
if (isset($_POST['AJOUTER'])){
    echo "</p> Produit_Ajout";
    echo "</p>";
    // Check and Get all $_POST['name_i_TableDBB']
    $inputNames = [ 'nom', 'id_cat_prod', 'prix', 'quantite', 'descript' ];   
    $inputValues = getPOST($inputNames);
    if( $inputValues === NULL ) { 
        // print "prob avec les var d'entrée."; 
        return NULL; 
    }
    
    // // Get Boolean 
    // $dispo  = (isset($_POST['dispo']))? 1: 0;
    // echo "</p> Produit_Ajout: dispo ";
    // var_dump($dispo);
    // array_push($inputNames, 'dispo');
    // array_push($inputValues, $dispo);

    // Get FILES 'imgFile'
    $inputFileVal = getUploadFILES('imgFile','./IMAGES/produits/');
    if( $inputFileVal === NULL ) { return NULL; }
    array_push($inputNames, 'imgFile');
    array_push($inputValues, $inputFileVal);
      
    // Check param inputNames inputValues
    // echo "</p>"; var_dump($inputNames);
    // echo "</p>"; var_dump($inputValues);
    // 
    createData('Produits', $inputNames, $inputValues);
    header('location: v2_produits.php');
}

// CRUD =========================================================
// Read
//
if (isset($_GET['id'])){
    echo "recuperation d'un produit";
    $produit_select = getDataId('Produits',$_GET['id']);
}

// CRUD =========================================================
// Update
//
// if (isset($_POST['MODIF_INFO'])){
//     echo "</p> INFO: modifier";
//     echo "</p>";
//     // Get values of all $_POST['...']
//     $inputNames = [ 'nom', 'prix', 'quantite' ];    
//     $inputValues = getPOST($inputNames);
//     if( $inputValues === NULL ) { return NULL; }
    
//     // Check param inputNames inputValues
//     echo "</p></p>"; var_dump($inputNames);
//     echo "</p></p>"; var_dump($inputValues);
//     // 
//     $info_updated = updateData('Information', $inputNames, $inputValues, $_POST['id']);
//     echo "</p></p>";
//     var_dump($info_updated);

//     echo "</p></p>"."Info: bien modifié";
//     header('location: ./v2_produits.php');
// }

if (isset($_POST['MODIFIER'])){
    echo "</p> Produit: modifier";
    echo "</p>";
    // Get values of all $_POST['...']
    $inputNames = [ 'nom', 'id_cat_prod', 'prix', 'quantite', 'descript' ];      
    $inputValues = getPOST($inputNames);
    if( $inputValues === NULL ) { return NULL; }
    
    // array_push($inputNames, 'dispo');
    // array_push($inputValues, $dispo);

    // Get FILES 'imgFile' if any modif
    $inputFileVal = getUploadFILES('imgFile','./IMAGES/produits/');
    if( $inputFileVal !== NULL ) { 
        array_push($inputNames, 'imgFile');
        array_push($inputValues, $inputFileVal);
    }

    // Check param inputNames inputValues
    echo "</p></p>"; var_dump($inputNames);
    echo "</p></p>"; var_dump($inputValues);
    // 
    $produit_select = updateData('Produits', $inputNames, $inputValues, $_POST['id']);
    echo "</p></p>";
    var_dump($produit_select);

    echo "</p></p>"."Produit: bien modifié";
    header('location: ./v2_produits.php');
}

// CRUD =========================================================
// Delete
if (isset($_POST['SUPPRIMER'])){
    echo "suppr d'un produit";
    deleteData('Produits',$_POST['id']);
    echo "supprimé";
    header('location: ./v2_produits.php');
}

?>