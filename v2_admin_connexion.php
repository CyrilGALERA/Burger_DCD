<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" sizes="16x16" href="./IMAGES/logo.png">
    <link rel="stylesheet" href="./CSS/styleG.css">
    <link rel="stylesheet" href="./CSS/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Praise&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <title>ADMIN: connexion</title>
</head>

<body>
    
    <?php
    include 'a_debug.php';
    include 'v0_header.php';
    ?>

    <main>
        <h2> ADMIN: connexion </h2>
        <div class="logadmin">
            <div class="logimg">
                <form name="form" id="formLogin" method="post" action="./v2_admin_accueil.php">
                    <label id="labeltxt" for="pseudo">Login</label>
                    <input autocomplete="off" type="text" name="pseudo" placeholder="Id Admin" /><br />
                    <label id="labeltxt" for="password">Mot De Passe</label>
                    <input autocomplete="off" type="password" name="password" placeholder="Mot de passe" /><br />
                    <input type="submit" name="valider" id="btn_auth" value="S'authentifier" />
            </div>
        </div>

    </main>

    </main>

    <?php include 'v0_footer.php';?>

</body>
</html>