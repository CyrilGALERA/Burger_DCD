-- Use script: sudo mysql < 0_DB_fill.sql;

-- -------------------------------------------------------------
-- DATABASE:    BDD_BurgerDCD;
-- TABLE:       Produits
--              CategorieProd
--              Administrateur
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use ClickAndCollect;
-- show tables;
-- describe Commande;
-- select * from Commande;
-- quit;
-- exit;
-- -------------------------------------------------------------

USE BDD_BurgerDCD;

-- TABLE Produit  -----------------------------

INSERT INTO CategorieProd (nom, descript)
    VALUES 
            ( "Restau", "Réservation du restaurant entier pour un événement"),
            ( "Salle", "Réservation de la salle1 pour un petit évènement"),
            ( "Table1", "Réservation pour 1 pers (avec une table pour 1 ou 2 pers)"),
            ( "Table2", "Réservation pour 2 pers (avec une table pour 1 ou 2 pers)"),
            ( "Burgers", "Burgers"),
            ( "Accompagnant", "Accompagnant"),
            ( "Boissons", "Boissons"),
            ( "Desserts", "Desserts"),
            ( "Menu", "Menu: burger, frite, boisson");

-- PRODUITS id 1 à 5: Cat 1 à 4: Restau Salle Table
INSERT INTO Produits (nom, imgFile, descript, prix, quantite, id_cat_prod)
    VALUES 
            ( "Restau",     "restau.png",   "Venez réserver le DCD Burger Restau pour vos évènements en famille, amis ou collègues", 2000, 1, 1),
            ( "Salle A Grill", "salle1.png", "Venez réserver la salle A Grill du DCD Burger pour vos évènements en famille, amis ou collègue", 500, 1, 2),
            ( "Salle B Grill", "salle1.png", "Venez réserver la salle B Grill du DCD Burger pour vos évènements en famille, amis ou collègue", 500, 1, 2),
            ( "Table 1 pers", "table01.jpg", "Réservation d'un table pour 1 personne", 0, 10, 3),
            ( "Table 2 pers", "table02.jpg", "Réservation d'un table pour 1 personne", 0, 30, 4);

-- PRODUITS id 6 à 9 : Cat 5 Burgers
INSERT INTO Produits (nom, imgFile, descript, prix, quantite, id_cat_prod)
    VALUES 
            ( "Burger 1", "b1.png", "Super Burger 1, avec bacon et ...", 5.50, 10, 5),
            ( "Burger 2", "b2.png", "Super Burger 2, avec ... et ...", 6.50, 10, 5),
            ( "Burger 3", "b3.png", "Super Burger 3, avec ???", 7.50, 10, 5),
            ( "Burger 4", "b4.png", "Super Burger 4, vraiement tres bon", 8.50, 10, 5);

-- PRODUITS id 10 a 18 : Cat 7 Boissons
INSERT INTO Produits (nom, prix, quantite, id_cat_prod)
    VALUES 
            ( "Coca Cola", 1.50, 50, 7),
            ( "Fanta", 2.00, 50, 7),
            ( "Orangina", 2.50, 50, 7),
            ( "Canada Dry", 1.50, 50, 7),
            ( "Boisson 5", 1.00, 50, 7),
            ( "Boisson 6", 9.50, 50, 7),
            ( "Boisson 7", 15, 50, 7),
            ( "Boisson 8", 20, 50, 7),
            ( "Boisson 9", 30, 50, 7);

-- PRODUITS id 19 a ... : Cat 9 Menu
INSERT INTO Produits (nom, imgFile, prix, quantite, id_cat_prod, descript)
    VALUES 
            ( "Menu 2 pers", "table01.jpg", 15.50, 50, 9, "Burger Poulet + Frites + Soda");


INSERT INTO PubProduits (id_prod, msg)
    VALUES 
            ( 1, "Réservez le restau pour vos événements, -25% de reduction jusqu'au 1 Mai!"),
            ( 6, "Un burger acheté, un burger offert!"),
            ( 7, "Un burger acheté, les frites offertes!"),
            ( 19, "-20% sur tous les les menus jusqu'aux 15 Mars!");
            







